package com.innovect.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 * Generic HTTP client to execute web service
 */
@Component
public class HTTPClient {

    private final Logger LOGGER = LoggerFactory.getLogger(HTTPClient.class);

    RestTemplate restTemplate;

    private String url;
    private HttpMethod httpMethod;
    private HttpHeaders httpHeaders;

    public HTTPClient() {
        restTemplate = new RestTemplate();
    }

    public HTTPClient(String url, HttpMethod httpMethod, HttpHeaders httpHeaders) {
        LOGGER.debug("Initializing http client.");
        restTemplate = new RestTemplate();
        this.url = url;
        this.httpMethod = httpMethod;
        this.httpHeaders = httpHeaders;
    }

    public ResponseEntity<String> invoke() {
        LOGGER.debug("Invoking http request for url: "+url);
        HttpEntity entity = new HttpEntity(this.httpHeaders);
        return restTemplate.exchange(this.url, this.httpMethod, entity, String.class);
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public HttpMethod getHttpMethod() {
        return httpMethod;
    }

    public void setHttpMethod(HttpMethod httpMethod) {
        this.httpMethod = httpMethod;
    }

    public HttpHeaders getHttpHeaders() {
        return httpHeaders;
    }

    public void setHttpHeaders(HttpHeaders httpHeaders) {
        this.httpHeaders = httpHeaders;
    }
}
