package com.innovect.utils;

/**
 * Class for constants.
 */
public final class Constants {

    private Constants() {

    }

    public final static String BASE_URL = "https://weather.cit.api.here.com/weather/1.0/report.json";
    public static final String PRODUCT_KEY = "product";
    public static final String PRODCUT_VALUE = "forecast_7days_simple";
    public static final String ZIPCODE_KEY = "zipcode";
    public static final String APPID_KEY = "app_id";
    public static final String APPID_VALUE = "aWsjb5Z3LjH4p6wtf6f7";
    public static final String APPCODE_KEY = "app_code";
    public static final String APPCODE_VALUE = "V0LaLmAtYn3ZBAMe0-fj2g";

    public static final String DAILY_FORECASTS = "dailyForecasts";
    public static final String FORECAST = "forecast";
    public static final String FORECAST_LOCATION = "forecastLocation";
    public static final String UTC_TIME = "utcTime";
    public static final String HIGH_TEMPERATURE = "highTemperature";
    public static final String LOW_TEMPERATURE = "lowTemperature";
}
