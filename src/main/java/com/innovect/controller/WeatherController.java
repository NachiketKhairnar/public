package com.innovect.controller;

import com.innovect.dto.ErrorResponse;
import com.innovect.dto.Response;
import com.innovect.service.WeatherService;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

/**
 * Rest controller to get tomorrow's temperature
 */
@RestController
@RequestMapping(value = "/weatherservice")
public class WeatherController {
    private static final Logger LOGGER = LoggerFactory.getLogger(WeatherController.class);

    @Autowired
    WeatherService weatherService;

    /**
     * Get tomorrow's temperature by zip-code
     * @param zipCode
     * @return
     */
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Get temperature for zip-code", response = Response.class)})
    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity getTemperature(@RequestParam(name = "zipcode") String zipCode) {
        ResponseEntity responseEntity;
        try {
            LOGGER.debug("Get temperature for zip-code: "+zipCode);
            responseEntity = new ResponseEntity(weatherService.getTemperature(zipCode), HttpStatus.OK);
            LOGGER.debug("Got temperature");
        } catch (HttpClientErrorException e) {
            LOGGER.debug("Invalid request data: "+zipCode);
            ErrorResponse errorResponse = new ErrorResponse();
            errorResponse.setError(e.getMessage());
            responseEntity = new ResponseEntity(errorResponse, e.getStatusCode());
        } catch (Exception e) {
            LOGGER.debug("Error occurred while getting temperature for zip-code: "+zipCode);
            ErrorResponse errorResponse = new ErrorResponse();
            errorResponse.setError(e.getMessage());
            responseEntity = new ResponseEntity(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }
}
