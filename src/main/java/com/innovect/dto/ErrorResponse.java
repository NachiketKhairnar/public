package com.innovect.dto;

/**
 * Data transfer class used in error case
 */
public class ErrorResponse {
    private String error;

    public ErrorResponse() {
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
