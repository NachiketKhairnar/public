package com.innovect.service;

import com.innovect.dto.Response;
import com.innovect.utils.HTTPClient;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import java.util.HashMap;
import java.util.Map;

import static com.innovect.utils.Constants.*;

/**
 * Service class to predict tomorrow's temperature
 */
@Service
public class WeatherService {

    private static final Logger LOGGER = LoggerFactory.getLogger(WeatherService.class);

    /**
     * Gets tomorrow's temperature from weather service.
     * @param zipCode
     * @return
     * @throws Exception
     */
    public Response getTemperature(String zipCode) throws Exception {
        LOGGER.debug("Executing third part request for zipcode: "+zipCode);
        try {
            HTTPClient httpClient = new HTTPClient(createURL(BASE_URL, getQueryParams(zipCode)), HttpMethod.GET, null);
            ResponseEntity<String> response = httpClient.invoke();
            if (response.getStatusCode() == HttpStatus.OK) {
                LOGGER.debug("Received response form third party service.");
                return getTemperatureFromJSON(response.getBody());
            } else {
                throw new Exception("Error occurred while communicating with third party service.");
            }
        } catch (HttpClientErrorException e) {
            if(e.getStatusCode().equals(HttpStatus.BAD_REQUEST)) {
                throw e;
            } else {
                throw new Exception("Error occurred while communicating with third party service.");
            }
        }
    }

    /**
     * Creates qualified rest service url.
     * @param baseURL
     * @param parameters
     * @return
     */
    private static String createURL(String baseURL, Map<String,String> parameters) {
        LOGGER.debug("Inside createURL");
        return baseURL.concat(parameters.entrySet().stream()
                .map(entry -> entry.getKey().concat("=").concat(entry.getValue()))
                .reduce((p1, p2) -> p1.concat("&").concat(p2))
                .map(s -> "?".concat(s))
                .orElse(""));
    }

    /**
     * Generates query param map.
     * @param zipCode
     * @return
     */
    private static Map<String, String> getQueryParams(String zipCode) {
        if(zipCode!=null) {
            Map<String, String> queryParams = new HashMap<>();
            queryParams.put(PRODUCT_KEY, PRODCUT_VALUE);
            queryParams.put(ZIPCODE_KEY, zipCode);
            queryParams.put(APPID_KEY, APPID_VALUE);
            queryParams.put(APPCODE_KEY, APPCODE_VALUE);
            LOGGER.debug("Inside getQueryParams");
            return queryParams;
        } else {
            throw new RuntimeException("Null value for zip-code");
        }
    }

    /**
     * Parse response json string and get min and max temperature.
     * @param jsonString
     * @return
     */
    private static Response getTemperatureFromJSON(String jsonString) {
        LOGGER.debug("Third party response json:" + jsonString);

        JSONObject parser = new JSONObject(jsonString);
        JSONObject tomorrowsData = parser.getJSONObject(DAILY_FORECASTS).getJSONObject(FORECAST_LOCATION)
                .getJSONArray(FORECAST).getJSONObject(1);
        LOGGER.debug("Tomorrow's temperature :" + tomorrowsData.toString());
        Response response = new Response();
        response.setDate(tomorrowsData.getString(UTC_TIME));
        response.setMaxTemperature(tomorrowsData.getDouble(HIGH_TEMPERATURE));
        response.setMinTemperature(tomorrowsData.getDouble(LOW_TEMPERATURE));
        LOGGER.debug("Generated response json");
        return response;
    }

}
