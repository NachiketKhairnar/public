import com.innovect.dto.Response;
import com.innovect.service.WeatherService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
public class TestService {

    @TestConfiguration
    static class EmployeeServiceImplTestContextConfiguration {

        @Bean
        public WeatherService employeeService() {
            return new WeatherService();
        }
    }

    @Autowired
    WeatherService weatherService;

    @Test
    public void test1() {
        try {
            Response response = weatherService.getTemperature("10025");
            assertNotNull(response.getDate());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test2() {
        try {
            Response response = weatherService.getTemperature("10");
        } catch (HttpClientErrorException e) {
            assertEquals(e.getStatusCode(), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test3() {
        try {
            Response response = weatherService.getTemperature("");
        } catch (HttpClientErrorException e) {
            assertEquals(e.getStatusCode(), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            assertNull(e.getMessage());
        }
    }

    @Test
    public void test4() {
        try {
            Response response = weatherService.getTemperature(null);
        } catch (Exception e) {
            assertNotNull(e.getMessage());
        }
    }

}
