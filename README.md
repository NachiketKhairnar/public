## Tomorrow's predicted temperature

Web service predicts tomorrow's temperature for US zip-codes. 
Web service needs HTTP GET request with a query parameter in the request as zipcode. 
Output of the web service is JSON with the date, min-temperature and max-temperature. 
Swagger UI is available for documentation through which you can see the request and response details.

## To get the code

Clone the repository:
```
$ git clone https://NachiketKhairnar@bitbucket.org/NachiketKhairnar/innovecture.git
```

## To run the application
From the command line with Maven:
```
$ cd innovecture
$ mvn clean install
$ cd innovecture\target
```

Deploy innovecture-1.0.war in the app server (e.g. Apache Tomcat).

Access the deployed web application at: http://<hostname>:<port>/innovecture-1.0/swagger-ui.html

Command to execute the web service from CLI:
curl -X GET --header 'Accept: application/json' 'http://<hostname>:<port>/innovecture-1.0/weatherservice?zipcode=10025' 
     